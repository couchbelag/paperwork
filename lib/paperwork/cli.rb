# frozen_string_literal: true

##
# globla namespace for paperwork
#
module Paperwork
    require "yaml"
    require "thor"
    require "paperwork"

    ##
    # command line interface for paperwork
    #
    class CLI < Thor
        attr_accessor :config

        CONFIG_FILE = "paperwork.yml"

        class_option :verbose, aliases: "-v", desc: "be verbose about the build", type: :boolean, default: false

        desc    "build",
                <<~DESC
                    creates documentation as defined in
                    #{CONFIG_FILE} and writes it to
                    '#{Paperwork::Config[:build_root]}/<name>/build'
                DESC
        def build
            build_internal "build"
        end

        desc    "rebuild",
                <<~DESC
                    removes previous builds and creates
                    documentation as defined in
                    #{CONFIG_FILE} and writes it to
                    '#{Paperwork::Config[:build_root]}/<name>/build'
                DESC
        def rebuild
            build_internal "rebuild"
        end

        desc    "server",
                "starts a aerver that serves documentation as defined in #{CONFIG_FILE}"
        def server
            build_internal "server"
        end

        protected

        def build_internal(task)
            setup_config
            setup_tasks
            invoke_tasks task
        end

        def setup_config
            unless File.exist?(CONFIG_FILE)
                raise Exception.new(
                    "#{CONFIG_FILE} not found. You need to create a configuration file first."
                )
            end

            yaml = YAML.load_file(CONFIG_FILE)
            self.config = yaml["config"]
            self.config["sources"] << CONFIG_FILE

            return unless self.config.nil?

            raise Exception.new(
                "No 'config' found in #{CONFIG_FILE}. You need to describe the build setup in a 'config' section first."
            )
        end

        def setup_tasks
            paperwork self.config["name"], sources: self.config["sources"]
        end

        def invoke_tasks(target)
            Rake::Task["paperwork:#{self.config["name"]}:set_verbose"].invoke if options[:verbose]
            Rake::Task["paperwork:#{self.config["name"]}:#{target}"].invoke
        end
    end
end
