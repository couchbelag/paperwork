# frozen_string_literal: true

module Paperwork
    ##
    # paperwork configuration
    #
    # currently available options are:
    #   :build_root     build directory [default: .work]
    #   :bundle_jobs    number of jobs for bundle install [default: 8]
    #   :verbose        verbose middleman build [default: false]
    #
    class Config
        attr_accessor :options

        def initialize
            self.options = {
                build_root: ".work",
                bundle_jobs: 8,
                verbose: false,
                custom_css: [],
                custom_js: []
            }
        end

        def [](key)
            self.options[key]
        end

        def []=(key, value)
            self.options[key] = value
        end

        def self.[](key)
            self.instance[key]
        end

        def self.[]=(key, value)
            self.instance[key] = value
        end

        def self.instance
            @@instance ||= self.new
        end
    end
end
