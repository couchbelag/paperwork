# frozen_string_literal: true

##
# global paperwork namespace
module Paperwork
    def paperwork_configure
        yield Paperwork::Config.instance
    end

    ##
    # creates the document named by 'task'. if a hash is provided
    # instead of task (like in regular task definitions in rake),
    # the first key is used as name and it's associated values are
    # treated as dependencies of that task.
    #
    def paperwork(task = nil, sources: [], **options)
        dependencies = []
        if task.nil?
            task = options.keys.first
            dependencies = options.delete(task)
        end
        doc = Paperwork::Tasks::Document.new(task, sources, *dependencies)
        doc.create_tasks
    end
end
