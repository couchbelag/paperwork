// This is where it all goes :)

import $ from "jquery";
import 'popper.js';
import 'bootstrap';
import mermaid from "mermaid";
import {getTableOfContents} from './toc';

mermaid.initialize({startOnLoad:true});

$(()=>{
  var sidebar = $(".toc.sidebar > ul")
  var topbar = $(".toc.topbar > ul")
  var toc = getTableOfContents();
  toc.insertTo(sidebar);
  toc.insertTo(topbar);
});
