import $ from "jquery";

var idCounter = 0;

class Section {
  constructor(element=null) {
    this.children = [];
    this.element = element;

    if (this.element) {
      this.anchor = "paperwork-h-" + ++idCounter;
    } else {
      this.element = $("body");
    }

    this.element.find(":header").each((index, elem)=>{
      this.add(new Section($(elem)));
    });
  }

  add(childSection) {
    this.children.push(childSection);
  }

  id() {
    return this.anchor;
  }

  level() {
    return this.element.prop("tagName").toLowerCase();
  }

  mark() {
    if (this.level() != "body") {
      // create anchor
      this.element.before("<div id='" + this.id() + "'></div>");
    }
    this.children.forEach((child)=>{
      child.mark();
    });
  }

  insertTo(target) {
    if (this.level() != "body") {
      // link to anchor
      target.append("<li class='toc-" + this.level() + "'><a href='#" + this.id() + "'>" + this.text() + "</a></li>");
    }
    this.children.forEach((child)=>{
      child.insertTo(target);
    });
  }

  text() {
    return this.element.text();
  }
}

function getTableOfContents() {
  var toc = new Section();
  toc.mark();
  return toc;
}

export { getTableOfContents };
