const path = require('path');

module.exports = {
    entry: './source/javascripts/site.js',
    devtool: 'source-map',

    resolve: {
        modules: [
            "node_modules",
            path.resolve(__dirname, '/source/javascripts')
        ],
    },

    output: {
        path: path.resolve(__dirname + '/.tmp/dist'),
        filename: 'javascripts/[name].js',
    },
};
