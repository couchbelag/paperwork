# frozen_string_literal: true


##
# this renderer is used to apply extensions to redcarpet
#
module DocRenderer
    require "middleman-core/renderers/redcarpet"
    require_relative "info_helpers"

    # This is a basic renderer for transforming markdown to HTML
    # inherit from this renderer to create more specific variants
    class Base < Middleman::Renderers::MiddlemanRedcarpetHTML
        include InfoHelpers

        # block level calls
        def block_code(code, language)
            if language == "mermaid"
                %(<div class="mermaid">#{code}</div>)
            else
                super
            end
        end

        def block_quote(quote)
            %(<div class="alert alert-info">#{quote}</div>)
        end

        # def block_html(raw_html)
        # end

        # def footnotes(content)
        # end

        # def footnote_def(content, number)
        # end

        # def header(text, header_level)
        # end

        # def hrule()
        # end

        # def list(contents, list_type)
        # end

        # def list_item(text, list_type)
        # end

        def paragraph(text)
            "<p>#{definition_list(text.strip)}</p>"
        end

        def table(header, body)
            <<~EOS_TABLE
                <table class='table table-striped table-hover table-sm'>
                    <thead>#{header}</thead>
                    <tbody>#{body}</tbody>
                </table>
            EOS_TABLE
        end

        # def table_row(content)
        # end

        # def table_cell(content, alignment)
        # end


        # span level calls
        # def autolink(link, link_type)
        # end

        # def codespan(code)
        #     %(<code>#{ERB::Util.html_escape(code)}</code>)
        # end

        # def double_emphasis(text)
        # end

        # def emphasis(text)
        # end

        def image(link, title, alt_text)
            <<~IMG
                <img src="#{relative_link link, scope.current_path}" alt="#{alt_text}" class="img-fluid">#{title}</img>
            IMG
        end

        # def linebreak()
        # end

        def link(link, title, content)
            super(link.gsub(/\.md$/, ".html"), title, content)
        end

        # def raw_html(raw_html)
        # end

        # def triple_emphasis(text)
        # end

        # def strikethrough(text)
        # end

        # def superscript(text)
        # end

        # def underline(text)
        # end

        # def highlight(text)
        # end

        # def quote(text)
        # end

        # def footnote_ref(number)
        # end

        private

        def definition_list(text)
            str = String.new
            re = /^[^:].*?(?:\R: .*)+/
            text.scan(re) do |match|
                list = match.split(":")
                str << "<dt>#{list[0]}</dt>"
                str << "<dd>#{list[1..-1].join("</dd><dd>")}</dd>"
            end

            if str.empty?
                text
            else
                "<dt>#{str}</dl>"
            end
        end
    end
end
