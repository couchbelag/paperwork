# frozen_string_literal: true

##
# helpers for info fields in documents
#
module InfoHelpers
    def text_for(**options)
        options.keys.map do |key|
            content_for key do
                options[key].to_s
            end
        end
    end

    def page_info(**options)
        content_for :page_infos do
            options.keys.map do |name|
                partial("layouts/info", locals: { name: name, value: options[name] })
            end.join
        end
    end

    def relative_link(stringifyable, current_path = current_page.path)
        relative = String.new
        dots = current_path.split(/[\/\\]/).size - 1
        dots.times{ relative += "../" }
        # This has been some nasty part to debug...
        # keep these comments for debugging session yet to come
        # puts "##################################"
        # puts "current_page.path: #{current_page.path}"
        # puts "stringifyable: #{stringifyable.to_s}"
        # puts "dots: #{dots}"
        # puts "relative link: #{relative + stringifyable.to_s}"
        # puts "##################################"
        relative + stringifyable.to_s
    end

    def paperwork?
        data.respond_to?(:paperwork)
    end

    def navbar?
        paperwork? && data.paperwork.respond_to?(:navbar)
    end

    def navbar_links
        nav = data.paperwork.navbar.links if navbar?
        nav ||= {}
        mapped = {}
        nav.each do |name, link|
            mapped[name] = relative_link(link)
        end
        mapped
    end

    def navbar_brand
        data.paperwork.navbar.brand if navbar?
    end

    def footer_text
        data.paperwork.footer if paperwork?
    end

    def toc?
        !data.paperwork.config.nil? &&
            !data.paperwork.config.toc.nil? &&
            data.paperwork.config.toc
    end
end
