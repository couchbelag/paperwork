# frozen_string_literal: true

require "lib/root_helpers"
helpers RootHelpers

require "lib/info_helpers"
helpers InfoHelpers

# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

activate :autoprefixer do |prefix|
    prefix.browsers = "last 2 versions"
end

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page "/*.xml", layout: false
page "/*.json", layout: false
page "/*.txt", layout: false

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

# configure :build do
#   activate :minify_css
#   activate :minify_javascript
# end
set :relative_links, true
activate :relative_assets
activate :livereload
activate :syntax, line_numbers: true


require "lib/doc_renderer"
helpers DocRenderer

set :markdown_engine,
    :redcarpet

set :markdown,
    fenced_code_blocks: true,
    smartypants: true,
    tables: true,
    highlight: true,
    superscript: true,
    autolink: true,
    underline: true,
    renderer: DocRenderer::Base

webpack_cmd = if build?
                  "./node_modules/webpack/bin/webpack.js --bail"
              else
                  "./node_modules/webpack/bin/webpack.js --watch -d eval --color"
              end

activate  :external_pipeline,
          name: :webpack,
          command: webpack_cmd,
          source: ".tmp/dist",
          latency: 1
