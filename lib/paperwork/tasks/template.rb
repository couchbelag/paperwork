# frozen_string_literal: true

module Paperwork
    ##
    # task generators for building with middleman
    module Tasks
        require "rake"

        ##
        # Task for generating middleman template directory for building
        #
        class Template < Paperwork::Tasks::Base
            attr_reader :template, :dir

            include Rake::DSL

            def initialize(name, *dependencies)
                @dir = File.join(Paperwork::Config[:build_root], name.to_s)
                @bundle = File.join(Paperwork::Config[:build_root], ".bundle")
                super(self.dir, File.dirname(self.dir), @bundle, *dependencies)

                BuildDir.new(@bundle)
                BuildDir.new(File.dirname(self.dir))
                @template = File.expand_path("middleman_template", __dir__)
            end

            def tasks
                directory self.name => self.dependencies do
                    cp_r template, self.dir
                    Dir.chdir(self.dir) do
                        cmd = "bundle install --path=../.bundle/gems --jobs 8 --without test development"
                        if defined? Bundler
                            Bundler.with_unbundled_env do
                                Process.spawn(cmd, out: :out, in: :in, err: :err)
                            end
                        else
                            Process.spawn(cmd, out: :out, in: :in, err: :err)
                        end
                        Process.spawn("yarn install", out: :out, in: :in, err: :err)
                        Process.waitall
                    end
                end
            end
        end
    end
end
