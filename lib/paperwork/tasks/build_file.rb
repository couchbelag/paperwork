# frozen_string_literal: true

module Paperwork
    ##
    # task generators for building with middleman
    module Tasks
        require "rake"

        ##
        # file task generator linking source files into the
        # middleman template directory
        #
        class BuildFile < Paperwork::Tasks::BuildDir
            attr_reader :source

            include Rake::DSL

            def initialize(name, source, *dependencies)
                super(name, source, *dependencies)
                @source = source
            end

            def tasks
                file self.name => self.dependencies do
                    cp self.source, self.name
                end
            end
        end
    end
end
