# frozen_string_literal: true

module Paperwork
    ##
    # task generators for building with middleman
    module Tasks
        require "rake"

        ##
        # task generator for root_helpers.rb that will
        # provide url path to the root page
        #
        class RootHelpers < Paperwork::Tasks::Base
            attr_reader :root

            include Rake::DSL

            def initialize(dir, root, *dependencies)
                super(
                    File.join(dir, "lib", "root_helpers.rb"),
                    dir,
                    *dependencies
                )
                Template.new(dir)

                @root = File.basename(root, ".md") + ".html"
            end

            def tasks
                file self.name => self.dependencies do
                    File.write(
                        name,
                        <<~ROOT_HELPERS
                            module RootHelpers
                                def root_path
                                    relative_link "#{self.root}"
                                end

                                def custom_css
                                    ["#{Paperwork::Config[:custom_css].join("\", \"")}"]
                                end

                                def custom_js
                                    ["#{Paperwork::Config[:custom_js].join("\", \"")}"]
                                end
                            end
                        ROOT_HELPERS
                    )
                end
            end
        end
    end
end
