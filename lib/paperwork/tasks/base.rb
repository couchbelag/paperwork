# frozen_string_literal: true

module Paperwork
    ##
    # task generators for building with middleman
    module Tasks
        ##
        # base class for all task generators. takes
        # care that every task is registered and
        # executed during build
        #
        class Base
            attr_reader :name, :dependencies

            def initialize(name, *dependencies)
                @name = name
                @dependencies = dependencies.compact
                self.register(self)
            end

            def register(task)
                registered_tasks[task.name] = task unless registered?(task.name)
            end

            def registered_tasks
                @@registered_tasks ||= {}
            end

            def registered?(name)
                registered_tasks.keys.include?(name)
            end

            def create_tasks
                registered_tasks.each do |_name, task|
                    task.tasks
                end
            end

            def self.explain
                list = @@registered_tasks.map do |_name, task|
                    "\t#{task.class}: #{task.name} => #{task.dependencies}"
                end
                list.join("\n")
            end
        end
    end
end
