# frozen_string_literal: true

module Paperwork
    ##
    # task generators for building with middleman
    module Tasks
        require "rake"

        # directory task generator creating dirs recursively
        class BuildDir < Paperwork::Tasks::Base
            include Rake::DSL

            def initialize(name, *dependencies)
                dir = File.dirname(name)
                if !dir.empty? && dir != "."
                    dependencies << dir
                    BuildDir.new(dir)
                end
                super(name, *dependencies)
            end

            def tasks
                directory self.name => self.dependencies
            end
        end
    end
end
