# frozen_string_literal: true

module Paperwork
    ##
    # task generators for building with middleman
    module Tasks
        require "rake"
        require "listen"

        ##
        # main task generator for building the document
        #
        class Document < Paperwork::Tasks::Base # rubocop:disable Metrics/ClassLength
            attr_reader :dir, :sources_map

            include Rake::DSL

            def initialize(name, sources, *dependencies)
                @dir = Paperwork::Tasks::Template.new(name).dir

                @sources_map = {}
                sources.each do |src|
                    dst = get_destination(File.join(self.dir, "source"), src)
                    BuildFile.new(dst, src)
                    sources_map[src] = dst
                    dependencies << dst
                end

                dependencies << RootHelpers.new(@dir, sources.first).name
                super(name, *dependencies)
            end

            def tasks
                tasks_build
                tasks_nested
            end

            private

            def get_destination(dst_base, src) # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
                nested_base = File.dirname(src)
                nested_base = if nested_base == "."
                                  dst_base
                              else
                                  File.join(dst_base, nested_base)
                              end

                ext = File.extname(src)
                case ext
                when ".md"
                    File.join(nested_base, File.basename(src)).gsub(".md", ".html.md")
                when ".scss"
                    Paperwork::Config[:custom_css] << File.basename(src, ext)
                    File.join(dst_base, "stylesheets", File.basename(src)).gsub(".scss", ".css.scss")
                when ".css"
                    Paperwork::Config[:custom_css] << File.basename(src, ext)
                    File.join(dst_base, "stylesheets", File.basename(src))
                when ".js"
                    Paperwork::Config[:custom_js] << File.basename(src, ext)
                    File.join(dst_base, "javascripts", File.basename(src))
                when ".yml"
                    File.join(dst_base, "..", "data", File.basename(src))
                else
                    File.join(dst_base, "images", File.basename(src))
                end
            end

            def tasks_build # rubocop:disable Metrics/AbcSize
                namespace :paperwork do
                    desc "build documentation for '#{self.name}'"
                    task self.name => self.dependencies do
                        verbose_flag = Paperwork::Config[:verbose] ? " --verbose" : ""
                        Dir.chdir(self.dir) do
                            cmd = "bundle exec middleman build#{verbose_flag}"
                            if defined? Bundler
                                Bundler.with_unbundled_env do
                                    Process.spawn(cmd, out: :out, in: :in, err: :err)
                                end
                            else
                                Process.spawn(cmd, out: :out, in: :in, err: :err)
                            end
                            Process.waitall
                        end
                        puts
                        puts "...Done!"
                        puts "build artifacts are located in #{File.join(self.dir, "build")}"
                    end
                end
            end

            def tasks_nested # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
                namespace :paperwork do # rubocop:disable Metrics/BlockLength
                    namespace self.name do # rubocop:disable Metrics/BlockLength
                        task build: ["paperwork:#{self.name}"]

                        desc "rebuild documentation from scratch for '#{self.name}'"
                        task rebuild: [:clean, :build]

                        desc "build documentation for '#{self.name}' with verbose output"
                        task build_verbose: [:set_verbose, :build]

                        task :set_verbose do
                            Paperwork::Config[:verbose] = true
                        end

                        desc "clean documentation build directory for '#{self.name}'"
                        task :clean do
                            rm_rf self.dir
                        end

                        desc "start middleman server for editing documents and get visual feedback with live reload"
                        task server: :build do
                            Dir.chdir(self.dir) do
                                cmd = "bundle exec middleman server"
                                if defined? Bundler
                                    Bundler.with_unbundled_env do
                                        Process.spawn(cmd, out: :out, in: :in, err: :err)
                                    end
                                else
                                    Process.spawn(cmd, out: :out, in: :in, err: :err)
                                end
                            end

                            listen_to_sources

                            Process.waitall
                        end
                    end
                end
            end

            def listen_to_sources
                sources = {}
                self.sources_map.each do |src, dst|
                    sources[File.absolute_path(src)] = dst
                end

                ignore_list = [/#{Paperwork::Config[:build_root]}/]
                Listen.to(Dir.pwd, ignore: ignore_list) do |modified, added, removed|
                    (modified + added + removed).each do |src|
                        dst = sources[src]
                        unless dst.nil?
                            puts "## updating #{src}"
                            FileUtils.cp src, dst
                        end
                    end
                end.start
            end
        end
    end
end
