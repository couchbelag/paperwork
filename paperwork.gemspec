# frozen_string_literal: true

require_relative "lib/paperwork/version"

Gem::Specification.new do |spec|
    spec.name          = "paperwork"
    spec.version       = Paperwork::VERSION
    spec.authors       = ["Tobias Schmid"]
    spec.email         = ["couchbelag@gmail.com"]

    spec.summary       = "Convert markdown documents to html pages"
    spec.homepage      = "https://gitlab.com/couchbelag/paperwork"
    spec.license       = "MIT"
    spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

    spec.metadata["allowed_push_host"] = "https://rubygems.org"

    spec.metadata["homepage_uri"] = spec.homepage
    spec.metadata["source_code_uri"] = "https://gitlab.com/couchbelag/paperwork.git"

    # Specify which files should be added to the gem when it is released.
    # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
    spec.files = Dir.chdir(File.expand_path(__dir__)) do
        %x(git ls-files -z).split("\x0").reject { |f| f.match(%r(^(test|spec|features)/)) }
    end
    spec.bindir        = "bin"
    spec.executables   = spec.files.grep(%r(^bin/)) { |f| File.basename(f) }
    spec.require_paths = ["lib"]

    spec.add_development_dependency "byebug", "~> 11.1.3"
    spec.add_development_dependency "rspec", "~> 3.0"
    spec.add_development_dependency "rspec_junit_formatter", "~> 0.4.1"
    spec.add_development_dependency "rubocop", "~> 0.87.1"
    spec.add_development_dependency "simplecov", "~> 0.18.5"

    spec.add_runtime_dependency "listen", "~> 3.4.1"
    spec.add_runtime_dependency "rake", "~> 12.3"
    spec.add_runtime_dependency "redcarpet", "~> 3.5.0"
    spec.add_runtime_dependency "thor", "~> 1.1.0"
end
