# frozen_string_literal: true

RSpec.describe Paperwork do
    it "has a version number" do
        expect(Paperwork::VERSION).not_to be nil
    end

    it "generates a presentation from README.md" do
        puts %x(rake paperwork:doc:rebuild -t)
    end

    it "is checked with rubocop" do
        puts %x(rubocop > #{workdir("rubocop.log")})
    end
end
