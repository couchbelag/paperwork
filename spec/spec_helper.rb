# frozen_string_literal: true

require "simplecov"

def workdir(*relative_paths)
    File.join(Dir.pwd, ".work", *relative_paths)
end
Dir.mkdir(workdir) unless Dir.exist?(workdir)

SimpleCov.start do
    enable_coverage :branch
    add_filter(%r(/test/))
    coverage_dir(workdir("coverage"))
end

require "bundler/setup"
require "paperwork"

RSpec.configure do |config|
    # Enable flags like --only-failures and --next-failure
    config.example_status_persistence_file_path = ".rspec_status"

    # Disable RSpec exposing methods globally on `Module` and `main`
    config.disable_monkey_patching!

    config.expect_with :rspec do |c|
        c.syntax = :expect
    end
end
