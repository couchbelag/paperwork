---
title: paperwork
---

# paperwork

[![w-10](https://gitlab.com/couchbelag/paperwork/badges/master/pipeline.svg)](https://gitlab.com/couchbelag/paperwork/-/commits/master)[![w-10](https://gitlab.com/couchbelag/paperwork/badges/master/coverage.svg)](https://couchbelag.gitlab.io/paperwork/coverage)


### The *paperwork gem* is an aproach to create appealing documentation from markdown documents.

Markdown is used throughout many software development projects because of its simple syntax. Though being simple markdown provides sufficient elements necessary to visualize most aspects of software. And it's integrated into many tools, like github, gitlab, Visual Studio Code,... most likely these are already part of nowadays development processes anyway.


Most developers want to focus on code and progress and not on documentation. *paperwork* tries to keep most of the publishing topics of documentation away from them. One important goal with this is to keep the markdown document independent from the target media. So it is possible to write documentation as HTML pages that can be scrolled, printed,... as you would do with usual web sites. On the other hand, markdown could also be transformed to presentations. If you're reading this in a presentation, you're already looking at one example of such a transformation. If you don't, have a look at the [generated presentation](https://couchbelag.gitlab.io/paperwork). The source file for the presentation is the [regular `README.md` file of the project](https://gitlab.com/couchbelag/paperwork/-/blob/master/README.md).


### Why would I want to transform my project documentation into a presentation?

Well... you wouldn't, I agree. Nevertheless I've been into situations where I created a presentation for a project and thought afterwards *"Damn, that would fit into the documentation perfectly!!"*... and I did it a second time. So the real benefit in this is to have a presentation that can be reused and integrated seamlessly into a project's documentation. In gitlab for example documentation and presentations would have same layout in the web UI, so you wouldn't notice what's presentation and what's documentation.

### *paperwork* is not another markdown renderer

*paperwork* is not simply a renderer like *redcarpet* (which is actually used as renderer within *paperwork*). It provides *"markdown API"* to interface with different media formats. Of course, *paperwork* is not limited to presentations and/or books. Any media could be addressed. The idea simply is *reduce, reuse, recycle*, write documentation once and spread it across multiple channels.


## Installation & Usage

### In a ruby application

Add this line to your application's Gemfile:

```ruby
gem 'paperwork'
```

And then execute:

```bash
$ bundle install
```


### In other languages

In other languages you will need to setup a `Gemfile` like the one above and a `Rakefile` for the build process:

```ruby
require "paperwork/tasks"

Paperwork.book do |p|
    p.title = "fancy title"
    p << "README.md"
end
```

Change into the directory where the Rakefile and the Gemfile (and of course the `README.md`) are located and execute:

```bash
$ bundle install
$ rake paperwork
```

If you'd like to know what tasks are shipped with *paperwork* do a `rake -T` to get a list of all tasks.

**For more information about how to use *paperwork* have a look into the [user's guide](guides/users_guide.md).**


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

If you'd like to contribute to the project, please have a look at the project's [developer guide](https://gitlab.com/couchbelag/gog/-/tree/2.0.0/developer-guide). The automated build process provides some information about the current *master* branch:

* [Example presentation from README.md (this file)](https://couchbelag.gitlab.io/paperwork)
* [code coverage report](https://couchbelag.gitlab.io/paperwork/coverage)

## Contributing

Bug reports and pull requests are welcome on gitlab at https://gitlab.com/couchbelag/paperwork.



## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
